/*
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 *  @file
 *
 * @brief Demo application of printing light sensor data on serialport(USB virtual comport)
 *   every three second, initiated by auto reloaded timer(freertos)
 *
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include "MAX_44009_ih.h"
#include "MAX_44009_ch.h"

/* system header files */
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BCDS_BSP_LED.h"
#include "BSP_BoardType.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_LightSensor.h"

/* local prototypes ********************************************************* */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
/** variable to identify interrupt configuration is done or not*/

/* global variables ********************************************************* */
/** variable to store timer handle*/
xTimerHandle LSD_printTimerHandle_gdt;
/* variable to store timer handle*/
uint16_t luxRawData = { LSD_ZERO };
uint32_t milliLuxData = { LSD_ZERO };
LightSensor_IntegrationTime_T integrationTime = LIGHTSENSOR_TIME_OUT_OF_RANGE;
uint32_t maxprintftickts = 0;
uint32_t maxticks = 0;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/* API documentation is in the interface header LSD_lightSensorDemo_ih.h*/

/* global functions ********************************************************* */
/**
 * @brief The function initializes MAX44009 sensor and set the sensor parameter from logger.ini
 */
void max_44009_init(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;

    /*initialize lightsensor*/
    returnValue = LightSensor_init(xdkLightSensor_MAX44009_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("lightsensorInit Success\r\n");
    }
    else
    {
        printf("lightsensorInit Failed\r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }

    }
    if (strcmp(MAX44009_int, "6.5") == 0)
    {
        integrationTime = LIGHTSENSOR_TIME_OUT_OF_RANGE;
    }
    else if (strcmp(MAX44009_int, "12.5") == 0)
    {
        integrationTime = LIGHTSENSOR_12P5MS;
    }
    else if (strcmp(MAX44009_int, "25") == 0)
    {
        integrationTime = LIGHTSENSOR_25MS;
    }
    else if (strcmp(MAX44009_int, "50") == 0)
    {
        integrationTime = LIGHTSENSOR_50MS;
    }
    else if (strcmp(MAX44009_int, "100") == 0)
    {
        integrationTime = LIGHTSENSOR_100MS;
    }
    else if (strcmp(MAX44009_int, "200") == 0)
    {
        integrationTime = LIGHTSENSOR_200MS;
    }
    else if (strcmp(MAX44009_int, "400") == 0)
    {
        integrationTime = LIGHTSENSOR_400MS;
    }
    else if (strcmp(MAX44009_int, "800") == 0)
    {
        integrationTime = LIGHTSENSOR_800_MS;
    }
    returnValue = LightSensor_setIntegrationTime(xdkLightSensor_MAX44009_Handle,
            integrationTime);
    if ((RETCODE_OK != returnValue)
            || (LIGHTSENSOR_TIME_OUT_OF_RANGE == integrationTime))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }
}

/**
 * @brief Read data from light sensor
 *
 * @param[in] pxTimer timer handle
 */
void max44009_getSensorValues(xTimerHandle pxTimer)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    (void) pxTimer; /* suppressing warning message */

    /* read Raw sensor data */
    returnValue = LightSensor_readRawData(xdkLightSensor_MAX44009_Handle,
            &luxRawData);
    if (RETCODE_OK != returnValue)
    {
        printf("lightsensorReadRawData Failed\r\n");
    }
    /* read sensor data in milli lux*/
    returnValue = LightSensor_readLuxData(xdkLightSensor_MAX44009_Handle,
            &milliLuxData);
    if (RETCODE_OK != returnValue)
    {
        printf("lightsensorReadInMilliLux Failed\r\n");
    }
}

/**
 *  @brief  The function to deinitialize
 *
 */
void max_44009_deInit(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    returnValue = LightSensor_deInit(xdkLightSensor_MAX44009_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("lightsensorDeinit Success\r\n");
    }
    else
    {
        printf("lightsensorDeinit Failed\r\n");
    }
}

/** ************************************************************************* */
