/*
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 *  @file        XDK_Datalogger_cc.c
 *
 *  @brief Data logger application
 *
 *  @detail Application to log all the defined sensors on SD-Card every 60 ms, initiated by auto reloaded Tasks(freertos)
 *          Default sampling of sensors are done for every 1 ms interval
 *
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "itoafunction/itoafunction.h"
#include "minIni/minIni.h"
#include "MAX_44009/MAX_44009_ch.h"
#include "MAX_44009/MAX_44009_ih.h"
#include "AKU_340/AKU_340.h"
#include "XDK_Datalogger_ih.h"
#include "BCDS_BSP_Mic_AKU340.h"

/* system header files */
#include <BCDS_Basics.h>
#include <stdio.h>
#include <string.h>

/* additional interface header files */
#include "BSP_BoardType.h"
#include "BCDS_BSP_Button.h"
#include "BCDS_BSP_LED.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#include "BCDS_SensorErrorType.h"
#include "BCDS_Accelerometer.h"
#include "XdkSensorHandle.h"
#include "BCDS_Assert.h"
#include "BCDS_SDCard_Driver.h"
#include "ff.h"
#include "em_cmu.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "task.h"
#include "Serval_Clock.h"
#include "ctype.h"
#include "AKU_340/acousticsignalprocessing_Component_Automatic.c"

/* constant definitions ***************************************************** */
#define FORCE_MOUNT  					UINT8_C(1)    /** Macro used to define the force mount */
#define DEFAULT_LOGICAL_DRIVE           ""            /** Macro used to define the default drive */
#define CUSTLOGBUFSIZE	256
#define SAMPLE_TASK_INTERVAL_IN_MS	            1
#define SAMPLE_TASK_INTERVAL_IN_TICKS    (SAMPLE_TASK_INTERVAL_IN_MS/portTICK_PERIOD_MS)
#define SD_TASK_INTERVAL_IN_MS	                60
#define SD_TASK_INTERVAL_IN_TICKS        (SD_TASK_INTERVAL_IN_MS/portTICK_PERIOD_MS)
#define TIMESTAMP_UNIT_IN_MS	                1
#define TIMESTAMP_UNIT_IN_TICKS          (TIMESTAMP_UNIT_IN_MS/portTICK_PERIOD_MS)
#define STRINGREPLACEBUFFER	                    256
#define CSTOVERHEAD	                            486
#define CSVOVERHEAD	                            255
#define JSONOVERHEAD	                        735
#define JSONOVERHEAD_PER_SENSOR                  70
#define BMA280_MAXBYTES                          30
#define BMG160_MAXBYTES                          30
#define BMI160_MAXBYTES                          60
#define BMM150_MAXBYTES                          36
#define BME280_MAXBYTES                          30
#define MAX44009_MAXBYTES                        16
#define AKU340_MAXBYTES           	             16
#define TIMESTAMP_MAXBYTES                       10
#define BYTESPERS                                18000 /**< max bytes per second that can be written to SD-Card*/


/* local variables ********************************************************** */
static Retcode_T SDC_diskInitStatus = SDCARD_NOT_INITIALIZED;
int8_t newFile = 1; /**< variable to check if a new file has to be added, set by Button pressed event*/
int8_t missingFile = 0; /**< variable to check if some INI-File is found on SD-Card, set if not*/
int8_t buttoncount = 0; /**< variable to store the amount of button pressed-events, to create next filename*/
int8_t addnewfile = 0; /**< variable to check the led-status by button pressed-events*/
TCHAR customHeader[CUSTLOGBUFSIZE] = { 0 }; /**< variable to store the first line of custlog.ini*/
TCHAR custstring[CUSTLOGBUFSIZE] = { 0 }; /**< variable to store the second line of custlog.ini*/
TCHAR filename[13] = { 0 }; //"log_%02d.csv"; /**< variable to store the actual filename*/
FATFS globalmnt; /**< variable for mount process of SD-Card*/
xSemaphoreHandle ReadSensorSemaphor = NULL; /**< Semaphore to lock/unlock the Ping/Pong-Buffer*/
configuration *conf = &config; /**< Pointer to the Sensor-configuration*/
Buffer pingBuffer = { 0 };
Buffer pongBuffer = { 0 };
Buffer *ActiveBuffer; /**< Pointer to the Buffer which is filled */
Buffer *BackBuffer; /**< Pointer to the Buffer to read out data */
FIL fileObject = { 0 }; /* File objects */
int8_t closefile = 0;
uint32_t fastestSamplingRate = 0; /**< variable to store the fastest sampling rate*/
uint32_t logActive;
uint32_t minimalTicks = 0; /**< variable to store the minimal Ticks according to fastestSamplingRate*/

/* global variables ********************************************************* */
configuration config = { { 0 }, { 0 }, { 0 }, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; /**< Object of the struct to store the sensor parameter defined in logger.ini*/

/* variable to store the configurable sensor parameter, used by sensor init*/
TCHAR bma280_bw[12] = { 0 };
TCHAR bmg160_bw[12] = { 0 };
TCHAR bmi160_accel_bw[12] = { 0 };
TCHAR bmi160_gyro_bw[12] = { 0 };
TCHAR bmm150_data[12] = { 0 };
TCHAR bme280_os[4] = { 0 };
TCHAR bme280_coeff[4] = { 0 };
TCHAR MAX44009_int[4] = { 0 };

/* global error flags to set by incorrect sensor init*/
/* inline functions ********************************************************* */

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(void * CmdProcessorHandle, uint32_t param2)
{
    BCDS_UNUSED(CmdProcessorHandle);
    BCDS_UNUSED(param2);

    GPIO_DriveModeSet(gpioPortA, gpioDriveModeLowest);
    GPIO_PinModeSet(gpioPortA, 0, gpioModePushPullDrive, 0);
    GPIO_PinOutClear(gpioPortA, 0);

    /*Call the RHC init API */
    vTaskDelay(5000);
    init();
}

/* local init-functions including System init and all Timer******************** */
extern void init(void)
{
    Retcode_T sdInitreturnValue;
    Retcode_T returnVal = RETCODE_OK;
    SDC_sdCardAppReturn_t csuReturnValue = 0;
    FRESULT fileSystemResult = 0;
    TaskHandle_t xHandle = NULL;
    TaskHandle_t xHandle1 = NULL;
    TaskHandle_t xHandle2 = NULL;
    const char *pcTextForTask1 = "TASK1 IS RUNNING";
    const char *pcTextForTask2 = "TASK2 IS RUNNING";
    const char *pcTextForTask3 = "TASK3 IS RUNNING";
    FILINFO initfno;
    BSP_Mic_AKU340_Connect();
    /* Initialize SD card */
    sdInitreturnValue = SDCardDriver_Initialize();
    ReadSensorSemaphor = xSemaphoreCreateMutex();
    if (ReadSensorSemaphor == NULL)
    {
        assert(0);
    }
    ActiveBuffer = &pingBuffer;
    BackBuffer = &pongBuffer;
    if (sdInitreturnValue != RETCODE_OK)
    { /* Debug fail case test SDC Init */
        assert(0);
    }
    else
    {
        if (SDC_diskInitStatus != SDCARD_DISK_INITIALIZED) /*SD-Card Disk Not Initialized */
        {
            SDC_diskInitStatus = SDCardDriver_DiskInitialize(SDC_DRIVE_ZERO); /* Initialize SD card */
        }
        if (SDCARD_DISK_INITIALIZED == SDC_diskInitStatus)
        {
            if (f_mount(&globalmnt, DEFAULT_LOGICAL_DRIVE, FORCE_MOUNT) != FR_OK)
            {
                assert(0);
            }
		}
	   else {
		   assert(0);
        }
    }
    if (SDC_APP_ERR_ERROR == csuReturnValue)
    { /* Debug fail case test for SDC Read/Write */
        assert(0);
    }
    else
    {
        getIniValues(); /**< get Sensor configuration*/
        if (FR_OK != f_stat("logger.ini", &initfno))
        {
            missingFile = 1;
        }
        if (strcmp(config.fileformat, "custom") == 0)
        {
            /**< search for custlog.ini on Sd-Card*/
            if (FR_OK == f_stat("custlog.ini", &initfno))
            {
                if (Count_CustLogLines() == 2) /**< only read the custlog lines if lines == 2*/
                {
                    customLog_LineRead(customHeader, custstring);
                }
                else
                {
                    returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
                    if (RETCODE_OK != returnVal)
                    {
                        printf("Turning on of RED LED failed \r\n");
                    }
                    strcpy(customHeader,
                            "custlog.ini not equals specification");
                    strcpy(custstring, "custlog.ini not equals specification");
                    printf("custlog.ini not equals specification\n");
                    exit(0);
                }
            }
            else
            {
                strcpy(customHeader, "CUSTLOG INI MISSING");
                strcpy(custstring, "CUSTLOG INI MISSING");
                missingFile = 1;
            }
        }
        else
        {
            customHeader[0] = '\0';
            custstring[0] = '\0';
        }
        scan_files(); /**< scan the files on SD-Card*/
        Sensor_init(); /**< Initialize the Sensors*/
        fileSystemResult = f_open(&fileObject, filename,
        FA_OPEN_EXISTING | FA_WRITE);
        if (fileSystemResult != FR_OK)
        {
            sprintf(filename, config.filename, buttoncount);
            fileSystemResult = f_open(&fileObject, filename,
            FA_CREATE_ALWAYS | FA_WRITE);
            if (fileSystemResult != FR_OK)
            {
                printf("Failed to create new data logging file on button press\r\n");
                returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of RED LED failed\r\n");
                }

                exit(0);
            }
        }
        fileSystemResult = f_lseek(&fileObject, f_size(&fileObject));
        if (fileSystemResult != FR_OK)
        {
            printf("Failed to expand file after successful opening of the data logger output file \r\n");
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed\r\n");
            }

        }
        if (xTaskCreate(normal_blink, (const char * const) pcTextForTask2,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle1) != pdTRUE)
        {
            assert(0);
        }
        if (xTaskCreate(SDC_writeBackBuffer, (const char * const) pcTextForTask3,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle2) != pdTRUE)
        {
            assert(0);
        }
        if (xTaskCreate(UpdateSensorValues, (const char * const) pcTextForTask1,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle) != pdTRUE)
        {
            assert(0);
        }
    }
}

/* API documentation is in the configuration header */
void PB0_Init(void)
{
    Retcode_T returnVal = RETCODE_OK;
    returnVal = BSP_Button_Connect();
    if (RETCODE_OK == returnVal)
    {
        returnVal = BSP_Button_Enable((uint32_t) BSP_XDK_BUTTON_1, PB0_InterruptCallback);
        BSP_Button_Enable((uint32_t) BSP_XDK_BUTTON_2, PB1_InterruptCallback);
    }
}

extern void Sensor_init(void)
{
    uint32_t bytesPerSample = TIMESTAMP_MAXBYTES;
    uint32_t validNumberOfBytes = 0;
    uint32_t samplesPerSecond = 0;
    uint32_t enabledSensorCounter = 0;/**< count the enabled sensor to calculate the json overhead*/

    if (config.max44009_enabled == 1)
    {
        max_44009_init();
        bytesPerSample += MAX44009_MAXBYTES; /**< add the max byte length of the MAX 44009 sensor parameter*/
        enabledSensorCounter++;
    }
#if BCDS_FEATURE_BSP_MIC_AKU340
    aku_340_init();
    bytesPerSample += AKU340_MAXBYTES; /**< add the max byte length of the AKU 340 sensor parameter*/
    enabledSensorCounter++;
#endif /* BCDS_FEATURE_BSP_MIC_AKU340 */
    if (strcmp(conf->fileformat, "json") == 0)
    {
        bytesPerSample += enabledSensorCounter * JSONOVERHEAD_PER_SENSOR;
    }
    validNumberOfBytes = BYTESPERS / bytesPerSample; /**< max number of bytes referring to BYTES PER Second and the sampling rate in ms*/
    samplesPerSecond = (validNumberOfBytes / 10) * 10;

    /**< set the sampling rate according to the rounded value of samples per second*/
    if (samplesPerSecond > 300)
    {
        fastestSamplingRate = samplesPerSecond / 100 * 100;
    }
    else if (samplesPerSecond > 100)
    {
        fastestSamplingRate = samplesPerSecond / 50 * 50;
    }
    else
    {
        fastestSamplingRate = samplesPerSecond;
    }
    minimalTicks = 1000 / fastestSamplingRate; /**< set the minimal valid Tick rate*/
    if (minimalTicks < 2)
    {
        minimalTicks = 2;
    }
    /**< set configuration to minimalTicks*/
    if (config.bma280_sampling_rate > fastestSamplingRate)
    {
        config.bma280_sampling_rate_timer_ticks = minimalTicks;
        config.bma280_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmg160_sampling_rate > fastestSamplingRate)
    {
        config.bmg160_sampling_rate_timer_ticks = minimalTicks;
        config.bmg160_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmi160_sampling_rate > fastestSamplingRate)
    {
        config.bmi160_sampling_rate_timer_ticks = minimalTicks;
        config.bmi160_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmm150_sampling_rate > fastestSamplingRate)
    {
        config.bmm150_sampling_rate_timer_ticks = minimalTicks;
        config.bmm150_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bme280_sampling_rate > fastestSamplingRate)
    {
        config.bme280_sampling_rate_timer_ticks = minimalTicks;
        config.bme280_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.max44009_sampling_rate > fastestSamplingRate)
    {
        config.max44009_sampling_rate_timer_ticks = minimalTicks;
        config.max44009_sampling_rate_remaining_ticks = minimalTicks;
    }
    printf("Maximum sampling rate for this configuration: %ld Hz\r\n",
            fastestSamplingRate);
    printf("Maximum ticks for this configuration: %ld ms\r\n", minimalTicks);
    printf("BMA280 ticks %ld ms \r\n", config.bma280_sampling_rate_timer_ticks);
    printf("BMG160 ticks %ld ms\r\n", config.bmg160_sampling_rate_timer_ticks);
    printf("BMI160 ticks %ld ms\r\n", config.bmi160_sampling_rate_timer_ticks);
    printf("BMM150 ticks %ld ms\r\n", config.bmm150_sampling_rate_timer_ticks);
    printf("BME280 ticks %ld ms\r\n", config.bme280_sampling_rate_timer_ticks);
    printf("MAX ticks %ld ms\r\n", config.max44009_sampling_rate_timer_ticks);
    PB0_Init();
}

/* End of Init-Section********************************************************************************** */

/* All Callback function for Timer and Interrupt events like button pressed********************************* */
/* API documentation is in the configuration header */
void PB0_InterruptCallback(uint32_t data)
{
	LP_T_VAL -= 0.1;
}

void PB1_InterruptCallback(uint32_t data)
{
	LP_K_VAL -= 0.1;
}

/* API documentation is in the configuration header */
void normal_blink(void *pvParameters)
{
    Retcode_T returnVal = RETCODE_OK;
    int8_t sdc_status = 0; /**< variable to check the SDC-Status, set if no SD-Card is found*/
    int8_t led_sdc_status = 0; /**< variable to check the led-status by missing SD-Card blink interval*/
    int8_t missingfile_led = 0; /**< variable to check the led-status by missing INI-File blink interval*/
    int8_t btnpressed_led = 0; /**< variable to check the led-status by button pressed-event*/
    int8_t normal_led_status = 0; /**< variable to check the led-status by normal run-mode*/
    (void) pvParameters;
    for (;;)
    {
        if (SDCARD_INSERTED == SDCardDriver_GetDetectStatus())
        {
            sdc_status = 0;
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed \r\n");
            }
        }
        else
        {
            sdc_status = 1;
        }
        if ((normal_led_status == 0) && (missingFile == 0) && (sdc_status == 0)
                && (addnewfile == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of ORANGE LED failed");
            }

            normal_led_status = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((normal_led_status == 1) && (missingFile == 0)
                && (sdc_status == 0) && (addnewfile == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of ORANGE LED failed");
            }

            normal_led_status = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 3000 / portTICK_RATE_MS);
        }
        else if ((addnewfile == 1) && (btnpressed_led == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_Y, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of YELLOW LED failed");
            }

            btnpressed_led = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 2000 / portTICK_RATE_MS);
        }
        else if ((addnewfile == 1) && (btnpressed_led == 1))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_Y, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of YELLOW LED failed");
            }

            btnpressed_led = 0;
            addnewfile = 0;
        }
        else if ((sdc_status == 1) && (led_sdc_status == 0))
        {
            printf(" User removed the SD card while logging, Please insert SD card and restart device \r\n");
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed \r\n");
            }

            led_sdc_status = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((sdc_status == 1) && (led_sdc_status == 1))
        {
            printf("\r\n");
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of RED LED failed \r\n");
            }

            led_sdc_status = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((missingFile == 1) && (missingfile_led == 0))
        {
            printf(" User selected custom logging configuration, but missed to keep 'custlog.ini' in SD card \r\n");
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed \r\n");
            }
            missingfile_led = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 1000 / portTICK_RATE_MS);
        }
        else if ((missingFile == 1) && (missingfile_led == 1))
        {
            printf("\r\n");
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of RED LED failed");
            }
            missingfile_led = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 1000 / portTICK_RATE_MS);
        }
    }
}

/* API documentation is in the configuration header */
void SDC_writeBackBuffer(void *pvParameters)
{
    Retcode_T returnVal = RETCODE_OK;
    (void) pvParameters;
    TickType_t xLastWakeTimeBufferWrite;
    const TickType_t xBufferWriteFrequency = SD_TASK_INTERVAL_IN_TICKS;
    int suceedwrite = 0;
    int fprintfret = 0;
    FRESULT fileSystemResult = 0;
    // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTimeBufferWrite = xTaskGetTickCount();
    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTimeBufferWrite, xBufferWriteFrequency);
        uint32_t Ticks = (portTickType) 1000;
        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if ((xSemaphoreTake(ReadSensorSemaphor, (portTickType)Ticks)
                == pdTRUE))
        {
            if (suceedwrite == 1)
            {
                BackBuffer->length = 0;
                BackBuffer->data[0] = 0;
                if (ActiveBuffer == &pingBuffer)
                {
                    ActiveBuffer = &pongBuffer;
                    BackBuffer = &pingBuffer;
                }
                else
                {
                    ActiveBuffer = &pingBuffer;
                    BackBuffer = &pongBuffer;
                }
                suceedwrite = 0;
            }
            if (xSemaphoreGive(ReadSensorSemaphor) != pdTRUE)
            {
                assert(0);
            }
        }
        else
        {
            assert(0);
        }
        if (closefile == 0)
        {
            fprintfret = f_printf(&fileObject, BackBuffer->data);
            if (fprintfret != -1)
            {
                f_sync(&fileObject);
                suceedwrite = 1;
            }
        }
        else
        {
            newFile = 1;
//            writeLogFooter(conf, &fileObject);
            BackBuffer->length = 0;
            BackBuffer->data[0] = 0;
            f_close(&fileObject);
            fileSystemResult = f_open(&fileObject, filename,
            FA_CREATE_ALWAYS | FA_WRITE);
            if (fileSystemResult != FR_OK)
            {
                returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of ORANGE LED failed");
                }

            }
            fileSystemResult = f_lseek(&fileObject, f_size(&fileObject));
            if (fileSystemResult != FR_OK)
            {
                returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of ORANGE LED failed");
                }

            }
            closefile = 0;
        }
    }
}

/* API documentation is in the configuration header */
void UpdateSensorValues(void *pvParameters)
{
    (void) pvParameters;
    TickType_t xLastWakeTimeSensorRead;
    TickType_t firstSampleTicks = 0;
    const TickType_t xSensorReadFrequency = SAMPLE_TASK_INTERVAL_IN_TICKS;
    int32_t buffstatus = 0;
    uint8_t valuesToWrite = 0;
    // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTimeSensorRead = xTaskGetTickCount();
    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTimeSensorRead, xSensorReadFrequency);
        valuesToWrite = sampleSensors(conf);
        if (valuesToWrite)
        {
            uint32_t Ticks = 1;

            if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
            {
                Ticks /= portTICK_RATE_MS;
            }
            if ((xSemaphoreTake(ReadSensorSemaphor, Ticks)
                    == pdTRUE))
            {
                buffstatus = BUFFSIZE - (ActiveBuffer->length + JSONOVERHEAD);
                if (buffstatus > 0)
                {
                    if ((newFile == 1))
                    {
                        firstSampleTicks = xLastWakeTimeSensorRead;
                        ActiveBuffer->length = 0;
                        ActiveBuffer->data[0] = 0;
                        writeSensorDataCsvHeader(conf);
                        newFile = 0;
                    }
                    writeLogEntry(conf, xLastWakeTimeSensorRead - firstSampleTicks);
                }
                if (xSemaphoreGive(ReadSensorSemaphor) != pdTRUE)
                {
                    assert(0);
                }
            }
        }
    }
}

/* End of Callback-Section********************************************************************************** */

/* All functions that be needed for read out logger.ini custlog.ini an the Write process to SD-Card  */
/* API documentation is in the configuration header */

FRESULT scan_files(void)
{
    FRESULT res;
    FILINFO fno;
    DIR dir;
    int i = 0;
    char *fn, *p; /* This function assumes non-Unicode configuration */
    TCHAR newestfile[MAX_FILE_NAME_LENGTH] = { 0 }; /**< variable to store the filename of the newest file on SD-Card*/
    static char path[MAX_PATH_LENGTH] = "";  /**< variable to store root path to SD-Card*/
    long temp_filenumber = 0;
    long filenumber = 0;

    res = f_opendir(&dir, path); /* Open the directory */
    if (res == FR_OK)
    {
        i = strlen(path);
        for (;;)
        {
            res = f_readdir(&dir, &fno); /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0)
                break; /* Break on error or end of dir */
            if (fno.fname[0] == '.')
                continue; /* Ignore dot entry */

            fn = fno.fname;
            if (fno.fattrib & AM_DIR)
            { /* It is a directory */
				sprintf(path + i, "/%s", fn);
				res = scan_files();
				path[i] = 0;
                if (res != FR_OK)
                {
                    break;
                }
            }
            else
            { /* It is a file. */
                /**< check for newer time stamp and a valid file format*/
                if (((strstr(fno.fname, "CSV") != NULL)
                        || (strstr(fno.fname, "CST") != NULL)
                        || (strstr(fno.fname, "JSN") != NULL)))
                {
                    strcpy(newestfile, fn);
                    p = newestfile;
                    while (*p)
                    { // While there are more characters to process...

                        if (isdigit((int) *p))
                        { // Upon finding a digit, ...
                            temp_filenumber = strtol(p, &p, 10); // Read a number, ...
                        }
                        else
                        { // Otherwise, move on to the next character.
                            p++;
                        }
                    }
                    if (temp_filenumber >= filenumber)
                    {
                        filenumber = temp_filenumber;
                        buttoncount = filenumber + 1;
                    }
                }
            }
        }
    }
    return res;
}

/* API documentation is in the configuration header */
int getIniValues(void)
{
    Retcode_T returnVal = RETCODE_OK;
    FRESULT ret = 0;
    uint8_t k;

    /* get the Sensor-Config-Parameters */
    /* Section [general] */
    ret = ini_gets("general", "filename", 0, config.filename, 13, "logger.ini");
    ret = ini_gets("general", "fileformat", "logger_ini_missing.txt",
            config.fileformat, 7, "logger.ini");
    ret = ini_gets("general", "dataformat", "logger_ini_missing.txt",
            config.dataformat, 5, "logger.ini");

    /* first get all strings for sensor config*/
    ret = ini_gets("bma280", "bandwidth", "logger_ini_missing.txt", bma280_bw,
            12, "logger.ini");
    ret = ini_gets("bmg160", "bandwidth", "logger_ini_missing.txt", bmg160_bw,
            12, "logger.ini");
    ret = ini_gets("bmi160", "bandwidth_accel", "logger_ini_missing.txt",
            bmi160_accel_bw, 12, "logger.ini");
    ret = ini_gets("bmi160", "bandwidth_gyro", "logger_ini_missing.txt",
            bmi160_gyro_bw, 12, "logger.ini");
    ret = ini_gets("bmm150", "data_rate", "logger_ini_missing.txt", bmm150_data,
            12, "logger.ini");
    ret = ini_gets("bme280", "oversampling", "logger_ini_missing.txt",
            bme280_os, 4, "logger.ini");
    ret = ini_gets("bme280", "filter_coefficient", "logger_ini_missing.txt",
            bme280_coeff, 4, "logger.ini");
    ret = ini_gets("MAX44009", "integration_time", "logger_ini_missing.txt",
            MAX44009_int, 4, "logger.ini");

    /* bma280 Sensor */
    config.bma280_enabled = ini_getl("bma280", "enabled", 0, "logger.ini");
    config.bma280_sampling_rate = ini_getl("bma280", "sampling_rate", 0,
            "logger.ini");
    config.bma280_sampling_rate_timer_ticks = (1000
            / (ini_getl("bma280", "sampling_rate", 0, "logger.ini")));
    config.bma280_range = ini_getl("bma280", "range", 0, "logger.ini");
    config.bma280_bandwidth = ini_getl("bma280", "bandwidth", 0, "logger.ini");
    config.bma280_sampling_rate_remaining_ticks =
            config.bma280_sampling_rate_timer_ticks;

    if (fastestSamplingRate < config.bma280_sampling_rate)
    {
        fastestSamplingRate = config.bma280_sampling_rate;
    }

    fprintf(stdout,
            " config.bma280_enabled: %ld\r\n config.bma280_bandwidth: %ld\r\n config.bma280_range: %ld\r\n config.bma280_sampling_rate_timer_ticks: %ld\r\n",
            config.bma280_enabled, config.bma280_bandwidth, config.bma280_range,
            config.bma280_sampling_rate_timer_ticks);

    /* bmg160 Sensor */
    config.bmg160_enabled = ini_getl("bmg160", "enabled", 0, "logger.ini");
    config.bmg160_sampling_rate = ini_getl("bmg160", "sampling_rate", 0,
            "logger.ini");
    config.bmg160_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmg160", "sampling_rate", 0, "logger.ini")));
    config.bmg160_bandwidth = ini_getl("bmg160", "bandwidth", 0, "logger.ini");
    config.bmg160_sampling_rate_remaining_ticks =
            config.bmg160_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmg160_sampling_rate)
    {
        fastestSamplingRate = config.bmg160_sampling_rate;
    }
    fprintf(stdout,
            " config.bmg160_enabled: %ld\r\n config.bmg160_bandwidth: %ld\r\n config.bmg160_sampling_rate_timer_ticks: %ld\r\n",
            config.bmg160_enabled, config.bmg160_bandwidth,
            config.bmg160_sampling_rate_timer_ticks);

    /* bmi160 */
    config.bmi160_enabled = ini_getl("bmi160", "enabled", 0, "logger.ini");
    config.bmi160_sampling_rate = ini_getl("bmi160", "sampling_rate", 0,
            "logger.ini");
    config.bmi160_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmi160", "sampling_rate", 0, "logger.ini")));
    config.bmi160_bandwidth_accel = ini_getl("bmi160", "bandwidth_accel", 0,
            "logger.ini");
    config.bmi160_bandwidth_gyro = ini_getl("bmi160", "bandwidth_gyro", 0,
            "logger.ini");
    config.bmi160_range = ini_getl("bmi160", "range", 0, "logger.ini");
    config.bmi160_sampling_rate_remaining_ticks =
            config.bmi160_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmi160_sampling_rate)
    {
        fastestSamplingRate = config.bmi160_sampling_rate;
    }
    fprintf(stdout,
            " config.bmi160_enabled: %ld\r\n config.bmi160_bandwidth_accel: %ld\r\n config.bmi160_bandwidth_gyro: %ld\r\n config.bmi160_range: %ld\r\n config.bmi160_sampling_rate_timer_ticks: %ld\r\n",
            config.bmi160_enabled, config.bmi160_bandwidth_accel,
            config.bmi160_bandwidth_gyro, config.bmi160_range,
            config.bma280_sampling_rate_timer_ticks);

    /* bmm150 Sensor */
    config.bmm150_enabled = ini_getl("bmm150", "enabled", 0, "logger.ini");
    config.bmm150_sampling_rate = ini_getl("bmm150", "sampling_rate", 0,
            "logger.ini");
    config.bmm150_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmm150", "sampling_rate", 0, "logger.ini")));
    config.bmm150_data_rate = ini_getl("bmm150", "data_rate", 0, "logger.ini");
    config.bmm150_sampling_rate_remaining_ticks =
            config.bmm150_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmm150_sampling_rate)
    {
        fastestSamplingRate = config.bmm150_sampling_rate;
    }
    fprintf(stdout,
            " config.bmm150_enabled: %ld\r\n config.bmm150_data_rate: %ld\r\n config.bmm150_sampling_rate_timer_ticks: %ld\r\n",
            config.bmm150_enabled, config.bmm150_data_rate,
            config.bmm150_sampling_rate_timer_ticks);

    /* bme280 sensor */
    config.bme280_enabled = ini_getl("bme280", "enabled", 0, "logger.ini");
    config.bme280_sampling_rate = ini_getl("bme280", "sampling_rate", 0,
            "logger.ini");
    config.bme280_sampling_rate_timer_ticks = (1000
            / (ini_getl("bme280", "sampling_rate", 0, "logger.ini")));
    config.bme280_oversampling = ini_getl("bme280", "oversampling", 0,
            "logger.ini");
    config.bme280_filter_coefficient = ini_getl("bme280", "filter_coefficient",
            0, "logger.ini");
    config.bme280_sampling_rate_remaining_ticks =
            config.bme280_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bme280_sampling_rate)
    {
        fastestSamplingRate = config.bme280_sampling_rate;
    }
    fprintf(stdout,
            " config.bme280_enabled: %ld\r\n config.bme280_oversampling: %ld\r\n config.bme280_filter_coefficient: %ld\r\n config.bme280_sampling_rate_timer_ticks: %ld\r\n",
            config.bma280_enabled, config.bme280_oversampling,
            config.bme280_filter_coefficient,
            config.bme280_sampling_rate_timer_ticks);

    /* max4409 sensor */
    config.max44009_enabled = ini_getl("MAX44009", "enabled", 0, "logger.ini");
    config.max44009_sampling_rate = ini_getl("MAX44009", "sampling_rate", 0,
            "logger.ini");
    config.max44009_sampling_rate_timer_ticks = (1000
            / (ini_getl("MAX44009", "sampling_rate", 0, "logger.ini")));
    config.max44009_integration_time = ini_getl("MAX44009", "integration_time",
            0, "logger.ini");
    config.max44009_sampling_rate_remaining_ticks =
            config.max44009_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.max44009_sampling_rate)
    {
        fastestSamplingRate = config.max44009_sampling_rate;
    }
    fprintf(stdout,
            " config.max44009_enabled: %ld\r\n config.max44009_integration_time: %ld\r\n config.max44009_sampling_rate_timer_ticks: %ld\r\n",
            config.max44009_enabled, config.max44009_integration_time,
            config.max44009_sampling_rate_timer_ticks);

    /*copy filename from config to process variable filename */
    for (k = 0; k < 13; k++)
    {
        filename[k] = config.filename[k];
    }
    if (strlen(filename) == 0)
    {
        printf("bad filename in logger.ini\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed \r\n");
        }
        exit(0);
    }
    return ret;
}

/* API documentation is in the configuration header */
int Count_CustLogLines()
{
    Retcode_T returnVal = RETCODE_OK;
    TCHAR CharBuffer[256] = { 0 };
    FIL FileObject;
    int lines = 0; /**< variable to store the amount of lines in custlog.ini**/

    if (f_open(&FileObject, "custlog.ini", FA_OPEN_EXISTING | FA_READ)
            == FR_OK)
    {
        while ((f_eof(&FileObject) == 0)) /**< get line by line until EOF*/
        {
            f_gets((char*) CharBuffer, sizeof(CharBuffer) / sizeof(TCHAR),
                    &FileObject);
            lines++; /**< increase lines on every line*/
        }
        f_close(&FileObject);
    }
    else
    {
        printf(" Either custlog.ini file is not available in SD card or fail to open it \r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed \r\n");
        }

    }
    return lines;
}

/* API documentation is in the configuration header */
int customLog_LineRead(TCHAR customHeader[], TCHAR custstring[])
{
    Retcode_T returnVal = RETCODE_OK;
    FIL cntObject; /* File objects */
    int l;
    int retval = 0;

    if (f_open(&cntObject, "custlog.ini", FA_OPEN_EXISTING | FA_READ)
            == FR_OK)
    {
        for (l = 0; (f_eof(&cntObject) == 0); l++)
        {
            if (l == 0) /* first line*/
            {
                f_gets(customHeader, CUSTLOGBUFSIZE, &cntObject); /**< get first line*/
            }
            if (l == 1) /* second line*/
            {
                f_gets(custstring, CUSTLOGBUFSIZE, &cntObject);/**< get second line*/
            }
        }
        f_close(&cntObject);
    }
    else
    {
        printf("Failed after reading 'custlog.ini' file \r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed \r\n");

        }
        retval = 1;
    }
    return retval;
}

/* API documentation is in the configuration header */
uint8_t sampleSensors(configuration *conf)
{
    uint8_t writeValues = 0;
    /* count down the the sampling_rate_remaining_ticks of every enabled Sensor
     * the sampling_rate_remaining_ticks according to the sampling_rate_timer_ticks which gets a value from 1 to 1000*/
    if (conf->max44009_enabled == 1)
    {
        conf->max44009_sampling_rate_remaining_ticks--;
        if (conf->max44009_sampling_rate_remaining_ticks < 1)
        {
            conf->max44009_sampling_rate_remaining_ticks =
                    conf->max44009_sampling_rate_timer_ticks;
            max44009_getSensorValues(NULL);
            writeValues = 1;
        }
    }
    return writeValues;
}

/* API documentation is in the configuration header */
char * stringReplace(char *search, char *replace, char *string)
{
    char *searchStart;
    char tempString[STRINGREPLACEBUFFER] = { 0 };
    int len = 0;
    uint32_t remainingBufSize = STRINGREPLACEBUFFER - strlen(string);
    /** check for search string
     */
    searchStart = strstr(string, search);
    if (searchStart == NULL)
    {
        return string;
    }
    /** temporary copy of the string
     */
    if (remainingBufSize >= strlen(replace))
    {
        strcpy((char *) tempString, string);

        /** set first section of the string
         */
        len = searchStart - string;
        string[len] = '\0';
        /** append second section of the string
         */
        strcat(string, replace);

        /** append third section of the string
         */
        len += strlen(search);
        strcat(string, (char*) tempString + len);
        return string;
    }
    else
    {
        return NULL;
    }
}

/* API documentation is in the configuration header */
void writeSensorDataCsvHeader(configuration *conf)
{


        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                " unit--timestamp[ms]");

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";max44009_bright[mLux]");
        }

    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            ";\n");
}

/* API documentation is in the configuration header */
void writeSensorDataCsv(uint64_t timestamp, configuration *conf)
{
    /* First check configured data format then write the data of all enabled sensors*/
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            "%llu", timestamp);

	if (conf->max44009_enabled == 1)
	{
		ActiveBuffer->length += sprintf(
				ActiveBuffer->data + ActiveBuffer->length, ";%ld",
				milliLuxData);
	}

#if BCDS_FEATURE_BSP_MIC_AKU340
    ActiveBuffer->length += sprintf(
            ActiveBuffer->data + ActiveBuffer->length, ";%d",
			aku340_getSensorValues());
#endif /* BCDS_FEATURE_BSP_MIC_AKU340 */
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            ";\n");
}


/* API documentation is in the configuration header */
void writeSensorDataCustom(char *customstring, uint64_t timestamp,
        configuration *conf)
{
    char buffer[10] = "";
    /*Write the Sensor date as specified in the custlog.ini*/
    char tempCustomString[STRINGREPLACEBUFFER] = { 0 };
    strcpy(tempCustomString, customstring);
    itoa(timestamp, buffer);
    stringReplace("%timestamp", buffer, tempCustomString); /* replace the string "%timestamp" with value of buffer*/

        if (conf->max44009_enabled == 1)
        {
            itoa(milliLuxData, buffer);
            stringReplace("%max44009_bright", buffer, tempCustomString);
        }

    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            "%s\n", tempCustomString);
    memset(tempCustomString, 0, STRINGREPLACEBUFFER);
}

/* API documentation is in the configuration header */
void writeLogEntry(configuration *conf, uint32_t currentTicks)
{
    uint32_t timestamp = 0;
    timestamp = ((currentTicks) / TIMESTAMP_UNIT_IN_TICKS); /*calculate the time stamp without the logStartTime-Offset*/
    writeSensorDataCsv(timestamp, conf);
}



/* End of user-Section********************************************************************************** */

/**
 *  @brief API to Deinitialize the PSD module
 */
extern void PSD_deinit(void)
{
    bma_280_deInit();
    max_44009_deInit();
    bme_280_deInit();
    bmg_160_deInit();
    bmi160_deInit();
    bmm_150_deInit();
    aku_340_deInit();
}

/** ************************************************************************* */
