/*
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 *  @file       XDK_Datalogger_ch.h
 *
 *  @brief  Configuration header XDK_Datalogger_cc.c
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef XDK_DATALOGGER_CH_H_
#define XDK_DATALOGGER_CH_H_

/* local interface declaration ********************************************** */

#include "ff.h"
/* local type and macro definitions */
#define SDC_WRITEREAD_DELAY 			UINT32_C(5000) 	 /**< Millisecond delay for WriteRead timer task */
#define SDC_WRITEREAD_BLOCK_TIME 		UINT32_C(0xffff) /**< Macro used to define block time of a timer*/
#define SDC_DETECT_SD_CARD_INSERTED 	UINT8_C(1)
#define SDC_SINGLE_BLOCK				UINT8_C(1)      /**< SD- Card Single block write or read */
#define SDC_DRIVE_ZERO				    UINT8_C(0)      /**< SD Card Drive 0 location */
#define SDC_PARTITION_RULE_FDISK	    UINT8_C(0)      /**< SD Card Drive partition rule 0: FDISK, 1: SFD */
#define SDC_AUTO_CLUSTER_SIZE		    UINT8_C(0)      /**< zero is given, the cluster size is determined depends on the volume size. */
#define SDC_SEEK_FIRST_LOCATION		    UINT8_C(0)      /**< File seek to the first location */

#define MAX_FILE_NAME_LENGTH              UINT8_C(13)
#define MAX_PATH_LENGTH                   UINT8_C(256)

/** structure required to collect Sensor parameter from INI-File on SD-Card */
typedef struct configuration
{
    TCHAR filename[13];
    TCHAR fileformat[7];
    TCHAR dataformat[5];

    uint32_t bma280_enabled;
    uint32_t bma280_sampling_rate_timer_ticks;
    uint32_t bma280_range;
    uint32_t bma280_bandwidth;
    uint32_t bma280_sampling_rate_remaining_ticks;
    uint32_t bma280_sampling_rate;

    uint32_t bmg160_enabled;
    uint32_t bmg160_sampling_rate_timer_ticks;
    uint32_t bmg160_bandwidth;
    uint32_t bmg160_sampling_rate_remaining_ticks;
    uint32_t bmg160_sampling_rate;

    uint32_t bmi160_enabled;
    uint32_t bmi160_sampling_rate_timer_ticks;
    uint32_t bmi160_bandwidth_accel;
    uint32_t bmi160_bandwidth_gyro;
    uint32_t bmi160_range;
    uint32_t bmi160_sampling_rate_remaining_ticks;
    uint32_t bmi160_sampling_rate;

    uint32_t bmm150_enabled;
    uint32_t bmm150_sampling_rate_timer_ticks;
    uint32_t bmm150_data_rate;
    uint32_t bmm150_sampling_rate_remaining_ticks;
    uint32_t bmm150_sampling_rate;

    uint32_t bme280_enabled;
    uint32_t bme280_sampling_rate_timer_ticks;
    uint32_t bme280_oversampling;
    uint32_t bme280_filter_coefficient;
    uint32_t bme280_sampling_rate_remaining_ticks;
    uint32_t bme280_sampling_rate;

    uint32_t max44009_enabled;
    uint32_t max44009_sampling_rate_timer_ticks;
    uint32_t max44009_integration_time;
    uint32_t max44009_sampling_rate_remaining_ticks;
    uint32_t max44009_sampling_rate;
} configuration;
/**/

/* local function prototype declarations */

/** @brief
 * 		Task to Update the enabled Sensors, by calling sampleSensors and write to the ActiveBuffer
 *
 *  @param[in] void *pvParameters
 * 		Rtos task should be defined with the type void *(as argument).
 */
void UpdateSensorValues(void *pvParameters);

/** @brief
 * 		Task to run all Blink interval
 *
 *
 *   @param[in] void *pvParameters
 */
void normal_blink(void *pvParameters);

/** @brief
 * 		The Callback function which is called by the Button pressed-Interrupt handler. Set global Flags, increase Button count and writes new filename to array filename.
 */
void PB0_InterruptCallback(uint32_t parameter);

void PB1_InterruptCallback(uint32_t parameter);

/** @brief
 * 		The function is used to Scan the Files on SD-Card and get the latest File and its filename.
 *      The current Number is extracted from the file name and is stored in button count variable
 *
 */
FRESULT scan_files(void);

/** @brief
 * 		The function returns the number of lines in the file custlog.ini
 *
 * @return 0 if file doesn't exist or empty, else number of lines in the file
 */
int Count_CustLogLines(void);

/** @brief
 * 		The function reads the first two lines of custlog.ini
 *
 * @param[out] header, string
 * @return 0 if ok, 1 if the file custlog.ini could not be opened
 */
int customLog_LineRead(TCHAR header[], TCHAR string[]);

/** @brief
 * 		The function used to get the Sensor config from logger.ini, and stores the values in the struct configuration
 * 		Processing the ini-file by ini-parser minIni
 *
 * @param[out] TCHAR header[], TCHAR string[]
 */
int getIniValues(void);

/* globale variables Store specific Sensor parameters. Set on Init of the Sensor-API */
extern configuration config;
extern TCHAR bma280_bw[12];
extern TCHAR bmi160_accel_bw[12];
extern TCHAR bmi160_gyro_bw[12];
extern TCHAR bmg160_bw[12];
extern TCHAR bmm150_data[12];
extern TCHAR bme280_os[4];
extern TCHAR bme280_coeff[4];
extern TCHAR MAX44009_int[4];

#endif /* XDK_DATALOGGER_CH_H_ */

/** ************************************************************************* */
