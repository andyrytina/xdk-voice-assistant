/*
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 * @file
 * @brief Application to log all the defined sensors on SD-Card periodically, triggered by a timer(free Rtos)
 *
 *
 *	@detail
 *  Application to log all the defined sensors on SD-Card every one ms, initiated by auto reloaded Tasks(freertos)
 *  Interface header for the XDK_Datalogger Module.
 *
 *  The interface header exports the following features:  PB0_Init,
 *                                                       Sensor_init,
 * 														 PSD_deinit,
 *														 SDC_writeBackBuffer,
 *														 sampleSensors,
 *														 stringReplace,	
 *														 writeSensorDataCsvHeader,
 *														 writeSensorDataCsv,
 *														 writeSensorDataJson,
 *														 writeSensorDataJsonHeader,
 *														 writeSensorDataJsonFooter,
 *														 writeSensorDataCustom,
 *														 writeLogEntry,
 *														 writeLogHeader,
 *														 writeLogFooter
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef XDK_DATALOGGER_IH_H_
#define XDK_DATALOGGER_IH_H_

#include <string.h>
#include "XDK_Datalogger_ch.h"
/* public interface declaration ********************************************* */
#define BUFFSIZE  19500


 /* Priorities */
#define TASK_PRIO_MAIN_CMD_PROCESSOR                (UINT32_C(1))
#define TASK_STACK_SIZE_MAIN_CMD_PROCESSOR          (UINT16_C(700))
#define TASK_Q_LEN_MAIN_CMD_PROCESSOR                (UINT32_C(10))

/* local function prototype declarations */
void appInitSystem(void * CmdProcessorHandle, uint32_t param2);

/* public type and macro definitions */
typedef enum SDC_sdCardAppReturn_e
{
    SDC_APP_ERR_DEINIT_FAILED = INT8_C(-1), /**< SD-Card DeInitialize failure */
    SDC_APP_ERR_INIT_FAILED = INT8_C(-2), /**< SD-Card Initialize failure */
    SDC_APP_ERR_ERROR = INT8_C(-3), /**< SD-Card Non-Error return */
    SDC_APP_ERR_WRITE_FAILURE = INT8_C(-4), /**< SD Card return write failure */
    SDC_APP_ERR_READ_FAILURE = INT8_C(-5), /**< SD Card return read failure */
    SDC_APP_ERR_NOTREADY_FAILURE = INT8_C(-6), /**< SD Card return Not ready failure */
    SDC_APP_ERR_NO_ERROR = INT8_C(0) /**< SD Card Error Return */
} SDC_sdCardAppReturn_t;

typedef struct
{
    int32_t length;
    char data[BUFFSIZE];
} Buffer;

/* public function prototype declarations */

/**
 * @brief Initialize Data Logger software module
 */
extern void init(void);
/**
 * @brief
 *      Init-function for Button1
 */
void PB0_Init(void);

/* public global variable declarations */

/* inline function definitions */

/**
 * @brief The function initializes Data logger which calls the individual sensor update functions
 * The functions updates raw and unit data of the sensors. Only sensors enabled in logger.ini will be initialized
 * Additional the Function downgrades the fastest sampling rate if its necessary to ensure a constant logging rate
 */
extern void Sensor_init(void);

/**
 *  @brief API to de-initialize the PSD module
 */
extern void PSD_deinit(void);

/* public global variable declarations */

/* inline function definitions */

/**
 * @brief
 *      Task to write the Data stored in Back buffer to SD-Card, and handle the create process for a new file.
 *
 *  @param[in] void *pvParameters
 */
void SDC_writeBackBuffer(void *pvParameters);

/**
 * \brief function for getting the sensor data, by calling the Sensor-API
 *  getting the data for all sensors and handling the individual sensor sample rates
 *
 *
 * @param   conf - pointer to configuration struct
 */
uint8_t sampleSensors(configuration *conf);

/**
 * \brief function string replace.
 *  search a substring in a string and replace it with a different string
 *
 * @param[in]   search - string to search
 * @param[in]   replace -string to replace with
 * @param[in]   string -string where replacement is made in
 *
 * @return  result string after replacement
 */
char * stringReplace(char *search, char *replace, char *string);

/**
 * \brief function for writing csv format header to the ActiveBuffer
 *
 *
 * @param[in]   conf - pointer to configuration struct
 */
void writeSensorDataCsvHeader(configuration *conf);

/**
 * \brief function for writing csv format sensor data to the ActiveBuffer
 *
 *
 * @param[in]   timestamp= timestamp in ms since start of logging, conf=pointer to configuration struct,
 */
void writeSensorDataCsv(uint64_t timestamp, configuration *conf);

/**
 * \brief function for writing json format sensor data to the ActiveBuffer
 *
 *
 * @param[in]   timestamp - time stamp in ms since start of logging, serial number=serial number, conf=pointer to configuration struct
 */
void writeSensorDataJson(uint64_t timestamp, uint64_t serialnumber,
        configuration *conf);

/**
 * \brief function for writing json format header to the ActiveBuffer
 *
 *
 * @param[in]    conf=pointer to configuration struct
 */
void writeSensorDataJsonHeader(configuration *conf);

/**
 * \brief function for writing json format footer
 *
 * @param[in]    buffer, the ActiveBuffer
 */
void writeSensorDataJsonFooter(FIL *fileObject);

/**
 * \brief function for writing custom format sensor data
 *
 *
 * @param[in]   customstring=user defined format string, time stamp= timestamp in ms since start of logging, conf=pointer to configuration struct,
 */
void writeSensorDataCustom(char *customstring, uint64_t timestamp,
        configuration *conf);

/**
 * \brief function for writing sensor data into the log file
 * depending on configuration raw or unit data and csv, json or custom file format
 *
 * @param[in]   customstring=user defined format string,  conf=pointer to configuration struct
 */
void writeLogEntry(configuration *conf, uint32_t timestamp);

/**
 * \brief function for writing header into the log file
 * depending on configuration csv, json or custom file format
 *
 * @param[in]   customheader=user defined header,  conf=pointer to configuration struct
 */
void writeLogHeader(configuration *conf, char *customheader);

/**
 * \brief function for writing footer into the log file
 * depending on configuration. Footer is written for json file format only
 *
 * @param[in]   conf=pointer to configuration struct
 */
void writeLogFooter(configuration *conf, FIL *fileObject);

#endif /* XDK_DATALOGGER_IH_H_ */
/** ************************************************************************* */
