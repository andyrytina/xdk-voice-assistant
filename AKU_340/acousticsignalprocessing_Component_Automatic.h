#ifndef _ASD_ACOUSTICSIGNALPROCESSING_COMPONENT_AUTOMATIC_H_
#define _ASD_ACOUSTICSIGNALPROCESSING_COMPONENT_AUTOMATIC_H_
/**
* @warning AUTOMATICALLY GENERATED FILE! DO NOT EDIT!
*
* @file    acousticsignalprocessing_Component_Automatic.h
*
* @author  Generated by ASCET-DEVELOPER V7.4.0 (Backend: Code Generator V7.4.0)
*
* @date    23.11.2018 16:59:32
*
* @brief   "acousticsignalprocessing_Component>>Automatic (>>esdl_Data_Default combined module/init code)"
*
*/


/*-----------------------------------------------------------------------------
 *    Include files
 *----------------------------------------------------------------------------*/

#include "esdl.h"
#include "SystemLib_Math_MathLib_Automatic.h"
#include "SystemLib_Transferfunction_Lowpass_LowpassKEnabled_Impl.h"
#include "SystemLib_Transferfunction_Lowpass_LowpassT_Impl.h"


/******************************************************************************
 * BEGIN: DEFINITION OF MEMORY CLASS STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 * ----------------------------------------------------------------------------
 * memory class:.................................'CAL_MEM'
 * ---------------------------------------------------------------------------*/
struct acousticsignalprocessing_Component_Automatic_CAL_MEM_SUBSTRUCT {
   float32 LP_K;
   float32 LP_T;
   sint32 MAXIMUM_LOW_SIGNALS_WITHIN_CLAP;
   sint32 MAXIMUM_SAMPLES_OF_CLAP;
};
/* ----------------------------------------------------------------------------
 * END: DEFINITION OF MEMORY CLASS STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 ******************************************************************************/


/******************************************************************************
 * BEGIN: DEFINITION OF MEMORY CLASS STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 * ----------------------------------------------------------------------------
 * memory class:.................................'RAM'
 * ---------------------------------------------------------------------------*/
struct acousticsignalprocessing_Component_Automatic_RAM_SUBSTRUCT {
   float32 dtY;
   float32 filteredSignal;
   sint32 highSignals;
   sint32 lowSignals;
   sint32 previousSample;
   sint32 sampleCounter;
   float32 sigin;
   float32 sigout;
   uint8 clapDetected;
   struct SystemLib_Transferfunction_Lowpass_LowpassKEnabled_Impl_RAM_SUBSTRUCT LowpassKEnabled_instance;
   struct SystemLib_Transferfunction_Lowpass_LowpassT_Impl_RAM_SUBSTRUCT lowpassT_instance;
};
/* ----------------------------------------------------------------------------
 * END: DEFINITION OF MEMORY CLASS STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 ******************************************************************************/


/******************************************************************************
 * BEGIN: DEFINITION OF MAIN STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 * ----------------------------------------------------------------------------
 * memory class:.................................'ROM'
 * ---------------------------------------------------------------------------*/
struct acousticsignalprocessing_Component_Automatic {
   struct SystemLib_Transferfunction_Lowpass_LowpassKEnabled_Impl LowpassKEnabled_instance;
   struct SystemLib_Transferfunction_Lowpass_LowpassT_Impl lowpassT_instance;
};
/* ----------------------------------------------------------------------------
 * END: DEFINITION OF MAIN STRUCTURE FOR MODULE 'acousticsignalprocessing_Component_Automatic'
 ******************************************************************************/

/* Following DEFINE signalizes the completion of definition                   */
/* of data structs for component: acousticsignalprocessing_Component_Automatic */
#define _acousticsignalprocessing_Component_Automatic_TYPE_DEF_




/* forward declaration of substruct variable 'acousticsignalprocessing_Component_CAL_MEM' */
/* containing 'CAL_MEM' memory class tree */
extern struct acousticsignalprocessing_Component_Automatic_CAL_MEM_SUBSTRUCT acousticsignalprocessing_Component_CAL_MEM;

/* forward declaration of substruct variable 'acousticsignalprocessing_Component_RAM' */
/* containing 'RAM' memory class tree */
extern struct acousticsignalprocessing_Component_Automatic_RAM_SUBSTRUCT acousticsignalprocessing_Component_RAM;

/* forward declaration of component variable 'acousticsignalprocessing_Component' */
/* containing 'ROM' memory class tree */
extern const struct acousticsignalprocessing_Component_Automatic acousticsignalprocessing_Component;

/******************************************************************************
 * BEGIN: declaration of global C functions defined by module acousticsignalprocessing_Component_Automatic
 ******************************************************************************/
void acousticsignalprocessing_Component_Automatic_checkHighSignal (/* IN    */ const float32 signal);
void acousticsignalprocessing_Component_Automatic_resetSignalCounter (void);



#endif /* _ASD_ACOUSTICSIGNALPROCESSING_COMPONENT_AUTOMATIC_H_ */
