/*----------------------------------------------------------------------------*/
/**
 *  @file
 *
 * @brief Demo application of printing audio sensor data on serialport(USB virtual comport)
 *   every three second, initiated by auto reloaded timer(freertos)
 *
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BCDS_BSP_LED.h"
#include "BSP_BoardType.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_BSP_Mic_AKU340.h"
#include "em_gpio.h"
#include "acousticsignalprocessing_Component_Automatic.h"
#include "SystemLib_Transferfunction_Lowpass_LowpassT_Impl.c"
#include "SystemLib_Transferfunction_Lowpass_LowpassKEnabled_Impl.c"


#define clapDetected_VAL (acousticsignalprocessing_Component_RAM.clapDetected)
#define dtY_VAL (acousticsignalprocessing_Component_RAM.dtY)
#define filteredSignal_VAL (acousticsignalprocessing_Component_RAM.filteredSignal)
#define highSignals_VAL (acousticsignalprocessing_Component_RAM.highSignals)
#define LowpassKEnabled_instance_REF (&(acousticsignalprocessing_Component.LowpassKEnabled_instance))
#define lowpassT_instance_REF (&(acousticsignalprocessing_Component.lowpassT_instance))
#define lowSignals_VAL (acousticsignalprocessing_Component_RAM.lowSignals)
#define LP_K_VAL (acousticsignalprocessing_Component_CAL_MEM.LP_K)
#define LP_T_VAL (acousticsignalprocessing_Component_CAL_MEM.LP_T)
#define MAXIMUM_LOW_SIGNALS_WITHIN_CLAP_VAL (acousticsignalprocessing_Component_CAL_MEM.MAXIMUM_LOW_SIGNALS_WITHIN_CLAP)
#define MAXIMUM_SAMPLES_OF_CLAP_VAL (acousticsignalprocessing_Component_CAL_MEM.MAXIMUM_SAMPLES_OF_CLAP)
#define previousSample_VAL (acousticsignalprocessing_Component_RAM.previousSample)
#define sampleCounter_VAL (acousticsignalprocessing_Component_RAM.sampleCounter)
#define sigin_VAL (acousticsignalprocessing_Component_RAM.sigin)
#define sigout_VAL (acousticsignalprocessing_Component_RAM.sigout)


/* local prototypes ********************************************************* */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */


static uint64_t ticks = 0;

static uint8_t pinIsSet = 0;

float32 ASD_DT_SCALED = 0.002F;


/* global variables ********************************************************* */
/** variable to store timer handle*/

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/* API documentation is in the interface header LSD_acousticSensorDemo_ih.h*/

/* global functions ********************************************************* */
/**
 * @brief The function initializes AKU340 sensor and set the sensor parameter from logger.ini
 */
void aku_340_init(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;

    /*initialize acoustic sensor*/
    returnValue = BSP_Mic_AKU340_Enable();
    if (RETCODE_OK == returnValue)
    {
        printf("acousticsensorEnable Success\r\n");
    }
    else
    {
        printf("acousticsensorEnable Failed\r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }
}


void doAction(){
	printf("command successful read. Do action.\r\n");
	BSP_LED_Switch(BSP_XDK_LED_Y, BSP_LED_COMMAND_TOGGLE);
	if(pinIsSet){
		GPIO_PinOutClear(gpioPortA, 0);
		pinIsSet = false;
	}else{
		pinIsSet = true;
		GPIO_PinOutSet(gpioPortA, 0);
	}


}

/**
 * @brief Read data from acoustic sensor
 *
 * @param[in] pxTimer timer handle
 */
int32_t aku340_getSensorValues()
{
	ticks++;
	int32_t returnValue = 0;
    /* read Raw sensor data */
    returnValue = BSP_Mic_AKU340_Sense();

    // ignore first 100 ticks, because of bad signal from the sensor
    if(ticks <= 100){
    	return returnValue;
    }

    sigin_VAL = (float32)returnValue;
    SystemLib_Transferfunction_Lowpass_LowpassT_Impl_compute(lowpassT_instance_REF, (float64)sigin_VAL, (float64)LP_T_VAL);

    sigout_VAL = (float32)SystemLib_Transferfunction_Lowpass_LowpassT_Impl_value(lowpassT_instance_REF);
    acousticsignalprocessing_Component_Automatic_checkHighSignal(sigout_VAL);
    if (ticks >= 1000 && highSignals_VAL >= 10)
    {
	  clapDetected_VAL = true;
	  acousticsignalprocessing_Component_Automatic_resetSignalCounter();
	  ticks = 150; // set tick cunter to a low number in order to ignore the next couple of samples
	  	  	  	   // ticks need to be at an certain threshold (ticks >= 1000) to execute the below action
	  doAction();
    }
    return returnValue;
}


/**
 *  @brief  The function to deinitialize
 *
 */
void aku_340_deInit(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;

    /*disconnect acoustic sensor*/
    returnValue = BSP_Mic_AKU340_Disconnect();
    if (RETCODE_OK == returnValue)
    {
        printf("acousticsensorDissconnect Success\r\n");
    }
    else
    {
        printf("acousticsensorDisconnect Failed\r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }

    }
    returnVal = BSP_Mic_AKU340_Disable();
    if (RETCODE_OK == returnValue)
    {
       printf("acousticsensorDisable Success\r\n");
    }
    else
    {
    	printf("acousticsensorDisable Failed\r\n");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }
}

/** ************************************************************************* */
