/*----------------------------------------------------------------------------*/
/**
 *  @file        
 *
 *  Configuration header for the AKU_340 module.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#include <stdint.h>
#include "BCDS_BSP_Mic_AKU340.h"
#include "SystemLib_Transferfunction_Lowpass_LowpassT_Impl.h"
#include "SystemLib_Transferfunction_Lowpass_LowpassKEnabled_Impl.h"
#include "esdl_deltaTimeDefs.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */


/**
 * @brief The function initializes light sensor and creates, starts timer task in autoreloaded mode
 * every three second which reads and prints the light sensor data
 */
void aku_340_init(void);

/**
 *  @brief  the function to deinitialize
 *
 */
void aku_340_deInit(void);
/**
 * @brief Read data from light sensor and print through the USB
 *
 * @param[in] pxTimer timer handle
 */
int32_t aku340_getSensorValues(void);

/* local module global variable declarations */

/* local inline function definitions */

/** ************************************************************************* */
